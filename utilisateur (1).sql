-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 21 juin 2021 à 10:11
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `inchclass`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `date_naiss` datetime NOT NULL,
  `pays` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `photo_profile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mot_pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `sexe`, `date_naiss`, `pays`, `telephone`, `photo_profile`, `email`, `mot_pass`) VALUES
(34, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'alexandrempocko@yahoo.com', 'ujhjj'),
(36, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'alexandrempobhgcko@yahoo.com', 'hyfty'),
(38, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'alexandrempobhgnjjjcko@yahoo.com', 'jhgug'),
(40, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'alexandrempobhgnhjnnjjjcko@yahoo.com', 'ghfhg'),
(42, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'alexaempobhgnhjnnjjjcko@yahoo.com', 'kcdfkdk'),
(43, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'alexaempobhgnhjnjjcko@yahoo.com', 'ofcockc'),
(45, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'aexaempobhgnhjnjjcko@yahoo.com', 'jhujhb'),
(47, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'aexapobhgnhjnjjcko@yahoo.com', 'jhnjnnj'),
(49, 'trehhh', 'gdjshbs', 'femme', '2021-06-11 00:00:00', 'Congo', '55555555555555555', 'polo.jpg', 'aexapobhgnhjnjiijcko@yahoo.com', 'jjj'),
(50, 'nnnnnn', 'hhhhhhhnnn', 'homme', '2021-06-12 00:00:00', 'Benin', '666524', 'polo.jpg', 'alexandrempocko@yahoo.fr', 'hbjj'),
(52, 'nnnnnnhh', 'hhhhhhhnnn', 'homme', '2021-06-12 00:00:00', 'Benin', '666524', 'polo.jpg', 'alexandrempockoik@yahoo.fr', 'hbjjn'),
(53, 'jnjnjnjn', 'jnjnknkl', 'homme', '2021-06-15 00:00:00', 'Camreroun', '444', 'polo.jpg', 'ddnjjkkdd@yahoo.fr', 'jn njnn'),
(54, 'hhbhh', 'hhhbhb', 'homme', '2021-06-08 00:00:00', 'Benin', '444444444444444444', 'polo.jpg', 'a@yqhoo.fr', 'nnnnnn'),
(55, 'nnjjn', 'hbjbnj', 'femme', '2021-06-03 00:00:00', 'Italie', '4444444444', 'polo.jpg', 'alexanlldrempocko@yahoo.com', 'jknkk'),
(56, 'nnjjn', 'hbjbnj', 'femme', '2021-06-03 00:00:00', 'Italie', '44444444444545', 'polo.jpg', 'alanlldrempocko@yahoo.com', 'mnikm'),
(59, 'nnjjn', 'hbjbnj', 'femme', '2021-06-03 00:00:00', 'Italie', '44444444444545', 'polo.jpg', 'alkmlanlldrempocko@yahoo.com', 'jknkmk'),
(61, 'nnjjn', 'hbjbnj', 'femme', '2021-06-03 00:00:00', 'Italie', '44444444444545', 'polo.jpg', 'alkmladnnlldrempocko@yahoo.com', 'dhdjbd'),
(62, 'nnjjn', 'hbjbnj', 'femme', '2021-06-03 00:00:00', 'Italie', '44444444444545', 'polo.jpg', 'alkmladnnlrempocko@yahoo.com', 'djndk'),
(63, 'huuuj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565665655265', 'polo.jpg', 'jfdjka@yqhoo.fr', 'djbdudi'),
(65, 'huuuj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565665655265', 'polo.jpg', 'jfdfdfjka@yqhoo.fr', 'uddsjdy'),
(66, 'huuuj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565665655265', 'polo.jpg', 'jfdfdfjhdudka@yqhoo.fr', 'jdbndjifdk'),
(68, 'huuuj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565665655265555', 'polo.jpg', 'jfdfdjfjfjhdudka@yqhoo.fr', 'fgjgnfjbnf'),
(69, 'huuujhjjj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565665655265555', 'WIN_20200902_21_08_59_Pro.jpg', 'jdfdjfjfjhdudka@yqhoo.fr', 'hbj'),
(70, 'huuujhjjj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565665655265555', 'polo.jpg', 'jdfdjfjfjhdudka@yqhopo.fr', '2hg23j53535353'),
(72, 'huuujhjjj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565665655265555', 'polo.jpg', 'jdfdjfjfjhdudka@yqh5opo.fr', '4524872754'),
(73, 'huuujhjjj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565665655265555', 'WIN_20200902_21_08_59_Pro.jpg', 'jdfdjfjdudka@yqh5opo.fr', 'jfnhjfdj55'),
(74, 'huuujhjjj', 'edlendklke', 'homme', '2021-06-04 00:00:00', 'Cameroun', '656565655265555', 'polo.jpg', 'jdfdjfjdudka@yqh5opo.frP', 'JFJFDJFJD'),
(76, 'massouka', 'alexandre', 'homme', '2021-06-12 00:00:00', 'Cameroun', '690981463', 'polo.jpg', 'hhhdddh@yqh.com', 'hhhhh'),
(78, 'massouka', 'alexandre', 'femme', '2021-06-12 00:00:00', 'Italie', '69098146355', 'WIN_20200902_21_08_59_Pro.jpg', 'hhhdnnnddh@yqh.com', 'nn nnn');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
