<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="build/css/intlTelInput.css">   
	<link rel="stylesheet" href="build/css/demo.css">
	<link rel="stylesheet" href="css/ionicons.min.css">
	<title>Document</title>
</head>
<body style="background-image:  url(images/eto.JPG); background-repeat: no-repeat; background-size: cover;">
	<div class="container">
		<div class="row">

			<div class="col-md-offset-3 col-md-6" style="border: 1px solid #000; background-color: rgba(0, 0, 0, 0.21);">
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<h3 style="font-weight: bold; color: black;"> <center > FORMULAIRE  INSCRIPTION </center> </h3>
					</div>
				</div>
				<form action="traitement.php" method="POST" style="color:#000;" enctype="multipart/form-data">
					<div class="form-group f_group">
						<label   for="">Nom</label>
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-user"></span>

							</span>

						  <input  type="text" placeholder="Nom"  class="form-control " name="nom" required="" pattern="[a-zA-Z]{}">
						  <span class="validity input-group-addon"></span>

						</div>
					</div>

					<div class="form-group f_group">
						<label for="">Prenom</label>
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-user"></span>

							</span>

						
						  <input type="text" name="prenom" placeholder="Prenom" class="form-control" required=""pattern="[a-zA-Z]{}">
						  <span class="validity input-group-addon"></span>
						</div>
				    </div>

				    <div class="form-group f_group">
						<label for="">date de naissance</label>
						<input type="date" name="date_naiss" placeholder="dd-mm-yyyy" class="form-control" required="">
					</div>
					<div class="form-group f_group">
						<label for="">Sexe</label>
						<select name="sexe" id="" class="form-control" required="" >
							<option value="" selected="selected" disabled="">choisissez votre sexe</option>		
							<option value="homme">homme</option>
							<option value="femme">femme</option>
						</select>
					</div>

					<div class="form-group f_group" >
						<label for="">Choix du pays</label>
						<select name="pays" id="" class="form-control" required="" >
							<option  selected="selected" value=""  disabled="">choisissez votre pays</option>		
							<option >Cameroun</option>
							<option >Benin</option>
							<option >Italie</option>
							<option >Congo</option>
							<option >Gabon</option>
							<option >RDC</option>
							<option >RCA</option>
							<option >Togo</option>
							<option >Mali</option>
						</select>

<!-- 
						 <h1> Telephone</h1>   
						 <form ><input class="form-control" id="phone" name="telephone" type="tel" >    </form>  <script src="build/js/intlTelInput.js"></script>   <script>     var input = document.querySelector("#phone"); 
						     window.intlTelInput(input, {        separateDialCode: true,       utilsScript: "build/js/utils.js",     });   </script>  -->
					</div>



					<div class="form-group  ">
						<label for="">TEL</label>
						<input type="tel" class="form-control" name="telephone" placeholder="Entrez votre numero ici" required=""pattern="[0-9]+" minlength="9" maxlength="20"> 
					</div>

					<div class="form-group f_group">
						<label for="">Email</label>
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-envelope"></span>

							</span>

						   <input type="email" name="email" placeholder="Email" class="form-control" required="" pattern="[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})">
						</div>   

					</div>

					<div class="form-group f_group" >
						<label for="">Mot de passe</label>
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-lock"></span>

							</span>
						    <input type="password" name="mot_pass" placeholder="choisissez votre mot de passe" class="form-control " required="">
						</div>    
					</div>
					<!-- <div class="form-group f_group" >
						<label for="">Confirmation de mot de passe</label>
						<div class="input-group">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-lock"></span>

							</span>

						    <input type="password" name="mot_pass2" placeholder="confirmez votre mot de passe" class="form-control "> 

						</div>    
						<small class="form-text text-muted" style="color: #000;"> fdfhyway xtdtyy</small>
					</div> -->
					<div class="form-group">
						<label for="photo">Photo de profil:</label>
                      <input type="file" name="photo_profile" id="photo" class="form-control" accept="image/*" onchange="loadFile(event)" required="">
                      <div class="col-md-offset-4 col-md-4" style="margin-top: 10px; margin-bottom: 10px; height: 120px; width: 120px; border-radius: 50%;">
                             <img id="pp" style="height: 120px; width: 120px; border-radius: 50%;" />
                      </div>
                    <script type="text/javascript">
                         var loadFile = function(event) {
                            var profil = document.getElementById('pp');
                              profil.src = URL.createObjectURL(event.target.files[0]);
                            };
                    </script>
					</div>
					<div class="form-group">
						<input type="submit" placeholder="Envoyer" class="form-control pull-right" style="width:80px;">
					</div>
				</form>
			</div>
		</div>
	</div>




	<script src="jquery/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>